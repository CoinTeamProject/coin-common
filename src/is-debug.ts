export function isDebug(): boolean {
  return toBool(process.env.DEBUG)
}

function toBool(item: any): boolean {
  switch (typeof item) {
    case 'boolean':
      return item
    case 'function':
      return true
    case 'number':
      return item > 0 || item < 0;
    case 'object':
      return !!item
    case 'string':
      const itemLC = item.toLowerCase()
      return ['true', 'yes', 'y', '1'].indexOf(itemLC) >= 0
    case 'symbol':
      return true
    case 'undefined':
      return false

    default:
      throw new TypeError('Unrecognised type: unable to convert to boolean')
  }
}
