import { isDebug } from './is-debug'

class FirebaseLogger {
  public readonly level: FirebaseLoggerLevel;

  constructor() {
    this.level = FirebaseLoggerLevel.INFO
  }

  public trace(...any: any) {
    if (this.level <= FirebaseLoggerLevel.INFO) {
      console.info(this.merge(any))
    }
  }

  public debug(...any: any) {
    if (this.level <= FirebaseLoggerLevel.DEBUG) {
      console.info(this.merge(any))
    }
  }

  public info(...any: any) {
    if (this.level <= FirebaseLoggerLevel.INFO) {
      console.info(this.merge(any))
    }
  }

  public warn(...any: any) {
    if (this.level <= FirebaseLoggerLevel.WARN) {
      console.warn(this.merge(any))
    }
  }

  public error(...any: any) {
    if (this.level <= FirebaseLoggerLevel.ERROR) {
      console.error(this.merge(any))
    }
  }
  private merge(any: any[]): string {
    return any.join(' ')
  }
}

enum FirebaseLoggerLevel {
  TRACE, DEBUG, INFO, WARN, ERROR, OFF,
}

function getLevel(): FirebaseLoggerLevel {
  let level = FirebaseLoggerLevel.INFO
  let levelStr = process.env.LOG
  if (levelStr !== undefined) {
    levelStr = levelStr.toUpperCase()
    const key = levelStr as keyof typeof FirebaseLoggerLevel;
    level = FirebaseLoggerLevel[key];
  }

  if (isDebug && level > FirebaseLoggerLevel.DEBUG) {
    level = FirebaseLoggerLevel.DEBUG
  }
  return level
}

export let logger = new FirebaseLogger()
